import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public  class Race {
    private final List<Horse> horses = Collections.synchronizedList(new ArrayList<>());
    private final List<Horse> finishList = Collections.synchronizedList(new ArrayList<>());

    public void addHorse(Horse horse){
        horses.add(horse);
    }

    public void  startRace(){
        for (Horse horse: horses){
            new Thread(horse).start();
        }
    }

    public synchronized void endRace(Horse horse){
        finishList.add(horse);
        System.out.println("Лошадь " + horse.getName() + " заняла место " + finishList.size());
        if (finishList.size() == horses.size()){
            displayResult();
        }
    }

    public void displayResult(){
        System.out.println("Результаты гонки:");
        for (int i = 0; i < finishList.size(); i++){
            System.out.println("Лошадь " + finishList.get(i).getName() + " заняла " + (i+1) + " место");
            if (i == 0){
                System.out.println("Поздравляем!");
            }
        }
    }
}
