public class Main {
    public static void main(String[] args) {
        Race race = new Race();
        race.addHorse(new Horse("Буцефал", 1, race));
        race.addHorse(new Horse("Спирит", 2, race));
        race.addHorse(new Horse("Зорька", 3, race));
        race.addHorse(new Horse("Кляча", 4, race));
        race.addHorse(new Horse("Иа", 5, race));


/*        for (int i = 1; i < 6; i++){
            race.addHorse(new Horse(i,race));
        }*/
        race.startRace();
    }
}