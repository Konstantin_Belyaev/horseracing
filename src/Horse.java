public class Horse implements Runnable {

    private String name;
    private final int ID;
    private Race race;

    private final int DISTANCE = 1000;
    private int position;

    @Override
    public void run() {
        while (position < DISTANCE){
            position += (int) (Math.random()*100)+1;
            if (position > DISTANCE){
                position  = DISTANCE;
            }
            System.out.println("Лошадь " + name + " прошла " + position + " метров. До финиша осталось " + (DISTANCE - position) + " метров." );
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        race.endRace(this);
    }


    public Horse(String name, int ID, Race race) {
        this.name = name;
        this.ID = ID;
        this.race = race;
    }

    public int getID() {
        return ID;
    }

    public Race getRace() {
        return race;
    }

    public int getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }
}
